var express = require('express');
var router = express.Router();
var userCtrl = require('../controllers/userController');
var app = express();
var cookieParser = require('cookie-parser');
app.use(cookieParser());

/* GET home page. */
router.get('/', function(req, res, next){
  res.render('index');
});

module.exports = router;
