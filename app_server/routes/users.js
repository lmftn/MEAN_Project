var express = require('express');
var router = express.Router();
var app = express();
var cookieParser = require('cookie-parser');
var userCtrl = require('../controllers/userController');
app.use(cookieParser());

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', function(req, res, next){
	userCtrl.userRegister(req, res, next);
});

// router.get('/login', function(req, res, next){
// 	res.send('login');
// })

router.post('/login', function(req, res, next){
	userCtrl.userLogin(req, res, next);
});

router.get('/logout', function(req, res, next){
	userCtrl.userLogout(req, res, next);
});

router.get('/list', function(req, res, next){
	userCtrl.usersList(req, res, next);
});

router.get('/profile', function(req, res, next){
	userCtrl.userProfile(req, res, next);
});

router.put('/profile', function(req, res, next){
	userCtrl.userUpdate(req, res, next);
});

router.delete('/profile', function(req, res, next){
	userCtrl.userDelete(req, res, next);
})

module.exports = router;
