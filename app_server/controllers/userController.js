var User = require('../models/userModel');
var validator = require('validator');
var bcrypt = require('bcrypt');
var express = require('express');
var cookieParser = require('cookie-parser');
var session = require('express-session'); 
var app = express();
const saltRounds = 10;

exports.userRegister = function (req, res, next) {
	if (!req.body.email || !req.body.username || !req.body.password || !req.body.pwdconfirmation) {
		res.status(400).json({ error: '400 error: All fields are required' });
	}
	else if (req.body.password != req.body.pwdconfirmation) {
		res.status(400).json({ error: '400 error: Password and Confirmation must match' });        
	}
	else {
		var newUser = new User({ email: req.body.email, username: req.body.username, password: req.body.password });
		newUser.save(function (err) {
			if (err) {
				console.log(err)
				if (err.code == 11000) {
					res.status(404).json({ error: '404 error: Duplicate key' });
				}
				else {
					res.status(404).json({ error: '404 error: Not found' });
				}
			}
			else {
				res.status(200).json('User created');
			}
		});
	}
}

exports.userLogin = function (req, res, next) {
	if (!req.body.email || !req.body.password) {
		res.status(400).json({ error: '400 error: All fields are required' });
	}
	else {
		User.findOne({ email: req.body.email }, function (err, user) {
			if (err || user == null) {
				res.status(404).json({ error: '404 error: Invalid credentials' });
			}
			else {
				bcrypt.compare(req.body.password, user.password, function (err, isMatch) {
					if (isMatch == true) {
						res.json('Successfully logged in');
					}
					else {
						res.status(404).json({ error: '404 error: Invalid credentials' });
					}
				})
			}
		});
	};
}

exports.userLogout = function(req, res, next){
	if(req.cookies['session']){
		res.clearCookie('session');
		res.json('Successfully logged out');
	}
	else{
		res.json('No one to log out');
	}
}

exports.usersList = function(req, res, next){
	//if(req.cookies['session']){
		var query = User.find();
		query.select().exec(function(err, data){
			res.json(data);
		})
	// }
	// else {
	// 	res.json('You need to be logged in');
	// }
}

exports.userProfile = function(req, res, next){
	if(req.cookies['session']){
		User.findOne({"_id": req.cookies['session']}, function(err, data){
			res.json(data);
		});
	}
	else {
		res.json('You need to be logged in');
	}
}

exports.userUpdate = function(req, res, next){
	if(req.cookies['session']){
		User.findOne({"_id": req.cookies['session']}, function(err, user){
			if(err){
				res.json('ERROR USERUPDATE');
			}
			else {
				user.username = req.body.username;
				user.email = req.body.email;
				user.password = req.body.password;
				user.save((err, user) => {
					if(err) {
						res.status(400).json(err);
					}
					else {
						res.json(user);
					}
				});
			}
		});
	}
	else {
		res.json('You need to be logged in');
	}
}

exports.userDelete = function(req, res, next){
	//if(req.cookies['admin']){
		User.findByIdAndRemove(req.body.id, (err, user) => {
			if(err){
				res.status(400).json("ERROR DELETE USER");
			}
			else {
				res.status(200).json('Successfully deleted');
			}
		})
	//}
}






