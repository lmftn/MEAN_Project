var mongoose = require('../models/db');
var bcrypt = require('bcrypt');
var Schema = mongoose.Schema;

var userSchema = new Schema({
	username: { type: String, required: true, unique: true },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
});

userSchema.pre('save', function (next) {
    var user = this;
    // hash the password only if modified or new
    if (!user.isModified('password'))
        return next();
    // generate salt
    bcrypt.genSalt(10, function(err, salt) {
        if (err)
            return next(err);
        // hash password with new salt
        bcrypt.hash(user.password, salt, function (err, hash) {
            if (err)
                return next(err);
            // override plaintext password with hash
            user.password = hash;
            next();
        });
    });
});

userSchema.methods.comparePassword = function (passwordtry,callback) {
    bcrypt.compare(passwordtry, this.password, function (err, isMatch) {
        if (err)
            return callback(err);
        callback(null, isMatch);
    });
};

var User = mongoose.model('User', userSchema, 'users');
module.exports = User;