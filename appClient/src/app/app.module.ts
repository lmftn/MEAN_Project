import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { UserService } from './user.service';
// import { RouteModule , Routes} from '@angular/router';
import { AppComponent } from './app.component';

// const appRoutes : Routes = [
//   {
//     path: ''
//   }
//
// ];
@NgModule({
 declarations: [
   AppComponent
 ],
 imports: [
   BrowserModule,
   FormsModule,
   HttpModule
 ],
 providers: [UserService],
 bootstrap: [AppComponent]
})

export class AppModule { }
