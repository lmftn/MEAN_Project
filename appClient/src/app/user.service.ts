import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import {User} from './toto';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class UserService {
	constructor (private http: Http) {}  

	getUser(): Promise<User[]> {
		return this.http.get('http://localhost:3000/users/list')
		.toPromise()
		.then(response => {
			return response.json() as User[];
		}).catch(this.handleError);
	}

	userLogin(req): Promise<object> {

		let head = new Headers({'Content-Type': 'application/json'});

		return this.http.post('http://localhost:3000/users/login', {email: req.value.email, password: req.value.password }, {headers: head})
		.toPromise()
		.then(response => {
			return response;
		}).catch(this.handleError);
	}

	// userLoginGet():


	private handleError(error: any): Promise<any> {
		console.error('An error occurred', error);
		return Promise.reject(error.message || error);
	}
}