import { Component, OnInit } from '@angular/core';
import { User } from './toto';
import { UserService } from './user.service';
import {NgForm} from '@angular/forms';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
 // providers: [UserService]
})

export class AppComponent implements OnInit{
	title = 'app works!';
	// user: User = {_id: 0, username: "test", password: "", email: "", __v: 0}
	users: User[] = []; 
	logged: object; 
	frm : any = {};
	cookie: string;

	constructor(private userService: UserService) { 
		console.log("loaded");
	}  

	getUser(): void {
		this.userService.getUser().then(users => this.users = users);
	}

	userLogin(frm: NgForm): void {
		this.userService.userLogin(frm).then(logged => this.logged = logged);
	}

	ngOnInit(): void {
		this.getUser();
		//this.userLogin();
	}
}

// export class SimpleFormComp {
//   onSubmit(f: NgForm) {
//   	console.log('SUBMIT');
//   }
//}